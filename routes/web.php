<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//login 
Route::get('/', function () {
    return view('login');
});
Route::post('/login','Auth\LoginController@login');


Route::get('/login', function () {
    return view('login');
});
Route::get('/dashboard', function () { 
    return view('Dashboard/Dashboard');
});

// user
Route::get('/userList', 'userController@userList');
Route::get('/addUser','userController@addUser'); 
Route::post('/addUserSubmit','userController@addUserSubmit'); 
Route::get('/editUser/{id}','userController@editUser'); 

Route::get('/deleteUser/{id}','userController@addUserSubmit'); 

// Company
Route::get('/companyList', function () {
    return view('Company/CompanyList');
});


Route::get('/addCompany', 'CompanyController@addCompany'); 
Route::post('/addCompanySubmit','CompanyController@addCompanySubmit'); 

Route::get('/addDirector', function () {
    return view('Directors/AddDirector');
});
Route::get('/addShareHolder', function () {
    return view('ShareHolders/AddShareHolder');
});
Route::get('/document', function () {
    return view('Document/Document');
});
Route::get('/companyInfo', function () {
    return view('CompanyInfo/CompanyInfo');
});
Route::get('/attendanceSheet', function () {
    return view('AttendanceSheet/AttendanceSheet');
});
Route::get('/MOM', function () {
    return view('MOM/MOM');
});

Route::get('/meetingList', function () {
    return view('Meeting/MeetingList');
});
Route::get('/addMeeting', function () {
    return view('Meeting/AddMeeting');
});
Route::get('/template', function () {
    return view('Template/Template');
});
Route::get('/UiComponent', function () {
    return view('Component/UiComponent');
});