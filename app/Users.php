<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Users extends Model
{
    public static function getUserData($email){
        $data = DB::table('users')->select('*')->where('email_id', $email)->get();
        return $data;
    }
    public static function getRole($role){
        $role = DB::table('roles')->select('roles')->where('id', $role)->get();
        return $role;
    }
    
    public static function getUsersList(){
        $usersList = DB::table('users')->select('*')->where('role', '3')->get();
        return $usersList;
    }
    public static function getRoles(){
        $roles = DB::table('roles')->select('*')->get();
        return $roles;
    }
    public static function insertUsers($request){
        $userName = $request->userName;
        $role = $request->selectRole;
        $password = $request->password;
        $email = $request->email;
        $phoneNumber = $request->phoneNumber;
        $status = $request->radio;
        $checkForExistingUser = DB::table('users')->select('*')->where('email_id', $email)->get();
        if($checkForExistingUser){
            $msg = "User already exists";
            return $msg;
        } else {
            $insert = DB::table('users')->insertGetId(['user_name' => "$userName", 'role'=>"$role", 'password'=>"$password",'email_id' => "$email" , 'phone_number' => "$phoneNumber" , 'status' => "$status"]);
            $success = "User Added Successfully";
            $err = "Failed to add User";
            if($insert){
                return $success;
            } else {
                return $err;
            }
        }
       
    }
         
}
