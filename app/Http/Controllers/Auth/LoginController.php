<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use App\Users;
use App\Login;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request) {
        $email = $request->emailId;
        $password = $request->password;
        $data = Users::getUserData($email);
        if(count($data)>0){
           foreach($data as $user){
               if($user->password == $password){
                $status = $user->status;
                if($status == 'inActive'){
                    $msg = "The User is inactive";
                    return Redirect::back()->with('status', $msg);
                   } else {
                        $roleId = $user->role;
                        $getRole = Users::getRole($roleId);
                        $role = $getRole[0]->roles;
                        Session::put("roleId",$roleId);
                        Session::put("role",$role);
                        Session::put("userName",$user->user_name);
                        Session::put("emailId",$user->email_id);
                        Session::put("userStatus",$user->status);
                        return view('Dashboard.Dashboard')->with(['roleId'=> $roleId,'role'=>$role]);
                }
               } else {
                $msg = "Incorrect Password";
                return Redirect::back()->with('status', $msg);
               }
           }
        } else {
            $msg = "User Does not exists";
            return Redirect::back()->with('status', $msg);
        }
        

    } 

}
