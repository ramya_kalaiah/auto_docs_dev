<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use App\Users;

class userController extends Controller
{
    public function userList() {
        $users = Users::getUsersList();
        return view('User/userList')->with("users",$users);
    } 

    public function addUser() {
        $roles = Users::getRoles();
        return view('User/addUser')->with("roles" , $roles);
    } 
    
    public function addUserSubmit(Request $request) {
        $insertUsers = Users::insertUsers($request);
        return Redirect::back()->with('status',$insertUsers);
    } 
    public function editUser(Request $request) {
        dd($request->id);
        $insertUsers = Users::insertUsers($request);
        return Redirect::back()->with('status',$insertUsers);
    } 
    
}
