<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class CompanyController extends Controller
{
    public function addCompany() {
         return view('Company/AddCompany');
    }
    public function addCompanySubmit(Request $request) {
        $cin_number = $request->cin_number;
        $company_name = $request->company_name;
        $company_logo = $request->company_logo;
        $incorporation_date = $request->incorporation_date;
        $incorporation_city = $request->incorporation_city;
        $website = $request->website;
        $address1 = $request->address1;
        $address2 = $request->address1;
        $tele_phone = $request->tele_phone;
        $city = $request->city;
        $state = $request->state;
        $country = $request->country;
        $pin_code = $request->tele_phone;
        $role_id = Session::get('roleId');
        $created_time = date("Y-m-d H:i:s");
        dd($created_time);
        
//        companyLogos
//        $extension = $request->file('company_logo')->getClientOriginalExtension();
//        dd($request->hasFile('company_logo'));
            
        //storing to folder
        $path = public_path('logo/');
//        $fileName = '1.jpg';
//        $company_logo->move($path,$company_logo); 
//          
//       dd(explode('.',$company_logo));
            
        $companyinsert = DB::table('company_info')
                ->insertGetId(['comp_name'=>$company_name,'comp_logo_path'=>$path,'comp_logo_filename'=>$company_logo,
                    'comp_CIN'=>$cin_number,'comp_incorporation_date'=>$incorporation_date,'comp_incorporation_city'=>$incorporation_city,
                    'comp_website'=>$website, 'comp_address'=>'','pin_code'=>$pin_code,'created_by'=> $role_id,'created_at'=>$created_time,'updated_by'=> $role_id,'updated_at'=>$created_time]);
        $last_insertId = $companyinsert;
        
        $addressInsert = DB::table('company_address')
                ->insertGetId(['comp_id'=>$last_insertId,'comp_address1'=>$address1,'comp_address2'=>$address2,'comp_telephone_no'=>$tele_phone,'comp_city'=>$city,
                    'comp_state'=>$state,'comp_country'=>$country]);
        
        $update = DB::table('company_info')->where('comp_id', $last_insertId)->update(['comp_address' => $addressInsert]);
        return Redirect::back()->with('msg','Company added successfully');
//        dd('saved');
    }
}
