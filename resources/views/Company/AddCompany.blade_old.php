@extends('Layout.app')

@section('title', 'Company')

@section('body')
    <div class="pt-32px pb-32px">
        <div class="container common-padding-32px">
            <div class="row">
                <div class="col-lg-12">
                    <ul id="progressbar">
                        <li class="active">
                            <a href="/addCompany" class="common-section__title fs-16px title-black">CompanyInfo</a>
                        </li>
                        <li>
                            <a href="/addDirector" class="common-section__title fs-16px title-black">Directors</a>
                        </li>
                        <li>
                            <a href="/addShareHolder" class="common-section__title fs-16px title-black">ShareHolders</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        @if(Session::has('msg'))
        <div class="col-md-11 no-padding margin-auto">
            <p class="alert alert-success col-md-6" style="margin:0 auto;">{{ Session::get('msg') }}</p>
        </div>
        @endif
        <div class="container box-shadow common-padding-32px">
            <div class="row ml-0px mr-0px align-items-center">
                <div class="col-xl-10">
                    <h5 class="mb-0px common-section__title">Add company</h5>
                </div>
                <div class="col-xl-2 text-right no-padding">
                    <a href="/addDirector" class="btn btn-primary custom-btn-portage-green custom-btn-portage-green--outline">
                        Next
                     <span>
                        <i class="fa fa-chevron-right" aria-hidden="true"></i>
                     </span>
                    </a>
                </div>
            </div>
            <div class="row mt-32px ml-0px mr-0px">
                <div class="col-lg-12 no-padding">
                    <form id="addCompanySubmit" method="post" action="{{ url('/addCompanySubmit') }}" >
                                {{ csrf_field() }}
                        <div class="form-group">
                            <div class="row ml-0px mr-0px align-items-center">
                                <div class="col-lg-4">
                                    <label class="common-label">CIN :</label>
                                    <div class="col-lg-12 no-padding">
                                        <input type="text" class="form-control common-input__from-control" id="cin_number" name="cin_number"
                                               placeholder="CIN" required/>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <label class="common-label">Company Name :</label>
                                    <div class="col-lg-12 no-padding">
                                        <input type="text" class="form-control common-input__from-control" id="company_name" name="company_name"
                                               placeholder="companyName"/>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <label class="common-label">Company Logo</label>
                                    <div class="col-lg-12 custom-file">
                                        <input type="file" class="form-control before_template" placeholder="Upload template" id="company_logo" name="company_logo" accept="image/x-png,image/jpeg,image/jpg">
                       
                                        <!--<input type="file" class="form-control common-input__from-control custom-file-input " id="company_logo" name="company_logo">-->
                                        <!--<label class="custom-file-label" for="customFile"></label>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row ml-0px mr-0px align-items-center">
                                <div class="col-lg-4">
                                    <label class="common-label">Incorporation Date :</label>
                                    <div class="col-lg-12 no-padding">
                                        <input type="text" class="form-control common-input__from-control" id="incorporation_date" name="incorporation_date"
                                               placeholder="Incorporation Date"/>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <label class="common-label">Incorporation City :</label>
                                    <div class="col-lg-12 no-padding">
                                        <input type="text" class="form-control common-input__from-control" id="incorporation_city" name="incorporation_city"
                                               placeholder="Incorporation City"/>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <label class="common-label">Website :</label>
                                    <div class="col-lg-12 no-padding">
                                        <input type="text" class="form-control common-input__from-control" id="website" name="website"
                                               placeholder="Website url"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row ml-0px mr-0px align-items-center">
                                <div class="col-lg-12">
                                    <p class="p-large common-section__title mb-0px">Registered Address</p>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row ml-0px mr-0px align-items-center">
                                <div class="col-lg-4">
                                    <label class="common-label">Address1 :</label>
                                    <div class="col-lg-12 no-padding">
                                        <input type="text" class="form-control common-input__from-control" id="address1" name="address1"
                                               placeholder="Address1"/>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <label class="common-label">Address2 :</label>
                                    <div class="col-lg-12 no-padding">
                                        <input type="text" class="form-control common-input__from-control" id="address2" name="address2"
                                               placeholder="Address2"/>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <label class="common-label">Tel No's :</label>
                                    <div class="col-lg-12 no-padding">
                                        <input type="number" class="form-control common-input__from-control" id="tele_phone" name="tele_phone"
                                               placeholder="TelNo's"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row ml-0px mr-0px align-items-center">
                                <div class="col-lg-4">
                                    <label class="common-label">City :</label>
                                    <div class="col-lg-12 no-padding">
                                        <input type="text" class="form-control common-input__from-control" id="city" name="city"
                                               placeholder="City"/>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <label class="common-label">State :</label>
                                    <div class="col-lg-12 no-padding">
                                        <input type="text" class="form-control common-input__from-control" id="state" name="state"
                                               placeholder="State"/>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <label class="common-label">Country :</label>
                                    <div class="col-lg-12 no-padding">
                                        <input type="text" class="form-control common-input__from-control" id="country" name="country"
                                               placeholder="Country"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row ml-0px mr-0px align-items-center">
                                <div class="col-lg-4">
                                    <label class="common-label">PINCode :</label>
                                    <div class="col-lg-12 no-padding">
                                        <input type="number" class="form-control common-input__from-control" id="pin_code" name="pin_code"
                                               placeholder="PIN Code"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row ml-0px mr-0px align-items-center justify-content-center" style="display: none;">
                            <div class="col-lg-8">
                                <div class="row justify-content-end">
                                    <div class="col-lg-2 text-right no-padding">
                                        <a href="/addCompany"
                                           class="btn btn-primary custom-btn-portage-green custom-btn-portage-green--outline" data-toggle="modal" data-target="#addAddress">
                                             <span>
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                             </span>
                                            Add Address
                                        </a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 mt-16px no-padding">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-striped" id="data-table"  style="width:100%">
                                               <thead>
                                                <tr class="table-header__container">
                                                    <th>Address1</th>
                                                    <th>Address2</th>
                                                    <th>City</th>
                                                    <th>PINCode</th>
                                                    <th class="width-80px">Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>#24, Novel Business Centre Off Outer Ring Road</td>
                                                        <td>BTM Layout</td>
                                                        <td>Bangalore</td>
                                                        <td>542678</td>
                                                        <td>
                                                            <div class="d-flex">
                                                                <div><a href="#" class="edit-icon" alt="edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></div>
                                                                <div><a href="#" class="trash-icon ml-8px" alt="delete"><i class="fa fa-trash" aria-hidden="true"></i></a></div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>#24, Novel Business Centre Off Outer Ring Road</td>
                                                        <td>BTM Layout</td>
                                                        <td>Bangalore</td>
                                                        <td>542678</td>
                                                        <td>
                                                            <div class="d-flex">
                                                                <div><a href="#" class="edit-icon" alt="edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></div>
                                                                <div><a href="#" class="trash-icon ml-8px" alt="delete"><i class="fa fa-trash" aria-hidden="true"></i></a></div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>#24, Novel Business Centre Off Outer Ring Road</td>
                                                        <td>BTM Layout</td>
                                                        <td>Bangalore</td>
                                                        <td>542678</td>
                                                        <td>
                                                            <div class="d-flex">
                                                                <div><a href="#" class="edit-icon" alt="edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></div>
                                                                <div><a href="#" class="trash-icon ml-8px" alt="delete"><i class="fa fa-trash" aria-hidden="true"></i></a></div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>#24, Novel Business Centre Off Outer Ring Road</td>
                                                        <td>BTM Layout</td>
                                                        <td>Bangalore</td>
                                                        <td>542678</td>
                                                        <td>
                                                            <div class="d-flex">
                                                                <div><a href="#" class="edit-icon" alt="edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></div>
                                                                <div><a href="#" class="trash-icon ml-8px" alt="delete"><i class="fa fa-trash" aria-hidden="true"></i></a></div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>#24, Novel Business Centre Off Outer Ring Road</td>
                                                        <td>BTM Layout</td>
                                                        <td>Bangalore</td>
                                                        <td>542678</td>
                                                        <td>
                                                            <div class="d-flex">
                                                                <div><a href="#" class="edit-icon" alt="edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></div>
                                                                <div><a href="#" class="trash-icon ml-8px" alt="delete"><i class="fa fa-trash" aria-hidden="true"></i></a></div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>#24, Novel Business Centre Off Outer Ring Road</td>
                                                        <td>BTM Layout</td>
                                                        <td>Bangalore</td>
                                                        <td>542678</td>
                                                        <td>
                                                            <div class="d-flex">
                                                                <div><a href="#" class="edit-icon" alt="edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></div>
                                                                <div><a href="#" class="trash-icon ml-8px" alt="delete"><i class="fa fa-trash" aria-hidden="true"></i></a></div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>#24, Novel Business Centre Off Outer Ring Road</td>
                                                        <td>BTM Layout</td>
                                                        <td>Bangalore</td>
                                                        <td>542678</td>
                                                        <td>
                                                            <div class="d-flex">
                                                                <div><a href="#" class="edit-icon" alt="edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></div>
                                                                <div><a href="#" class="trash-icon ml-8px" alt="delete"><i class="fa fa-trash" aria-hidden="true"></i></a></div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group mb-0px">
                            <div class="row ml-0px mr-0px align-items-center">
                                <div class="col-lg-12">
                                    <div class="row justify-content-end">
                                        <div class="col-lg-1">
                                            <button type="submit"
                                                    class="btn btn-primary btn-block custom-btn-portage-green custom-btn-portage-green--outline" id="save-company">
                                                Save
                                            </button>
                                        </div>
                                        <div class="col-lg-1">
                                            <a href="/companyList"
                                               class="btn btn-danger btn-block custom-btn-danger custom-btn-danger--outline">
                                                Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="addAddress" tabindex="-1" role="dialog" aria-labelledby="addAddress" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header common-modal-header__container">
                    <h6 class="modal-title common-section__title title-white">Add Address</h6>
                    <button type="button" class="close common-modal-close__container" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body common-modal-body__container">
                   <div class="row ml-0px mr-0px align-items-center justify-content-center">
                       <div class="col-lg-12 no-padding">
                           <form id="addCompany" method="post" action="{{ url('/addCompanySubmit') }}" >
                                {{ csrf_field() }}
                               <div class="form-group">
                                   <div class="row ml-0px mr-0px align-items-center">
                                       <div class="col-md-3 text-right">
                                           <label class="mb-0px common-label">Address1 :</label>
                                       </div>
                                       <div class="col-md-9">
                                           <input type="text" class="form-control common-input__from-control" placeholder="address1"/>
                                       </div>

                                   </div>
                               </div>
                               <div class="form-group">
                                   <div class="row ml-0px mr-0px align-items-center">
                                       <div class="col-md-3 text-right">
                                           <label class="mb-0px common-label">Address2 :</label>
                                       </div>
                                       <div class="col-md-9">
                                           <input type="text" class="form-control common-input__from-control" placeholder="address2"/>
                                       </div>

                                   </div>
                               </div>
                               <div class="form-group">
                                   <div class="row ml-0px mr-0px align-items-center">
                                       <div class="col-md-3 text-right">
                                           <label class="mb-0px common-label">City :</label>
                                       </div>
                                       <div class="col-md-9">
                                           <input type="text" class="form-control common-input__from-control" placeholder="City"/>
                                       </div>

                                   </div>
                               </div>
                               <div class="form-group">
                                   <div class="row ml-0px mr-0px align-items-center">
                                       <div class="col-md-3 text-right">
                                           <label class="mb-0px common-label">PINCode :</label>
                                       </div>
                                       <div class="col-md-9">
                                           <input type="text" class="form-control common-input__from-control" placeholder="PINCode"/>
                                       </div>

                                   </div>
                               </div>
                           </form>
                       </div>
                   </div>
                </div>
                <div class="modal-footer common-modal-footer__container">
                    <div class="row mr-0px ml-0px align-items-center justify-content-end">
                        <div class="col-lg-12 no-padding">
                            <div class="row mr-0px ml-0px align-items-center justify-content-end">
                                <div class="col-lg-3">
                                    <button type="button" class="btn btn-primary btn-block custom-btn-portage-green custom-btn-portage-green--outline">Save</button>
                                </div>
                                <div class="col-lg-3">
                                    <button type="button" class="btn btn-danger btn-block custom-btn-danger custom-btn-danger--outline" data-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script src="../../js/company.js"></script>
@endsection