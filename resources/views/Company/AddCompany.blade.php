@extends('Layout.app')

@section('title', 'Company')

@section('body')
    <div class="pt-32px pb-32px">
        <div class="container common-padding-32px">
            <div class="row">
                <div class="col-lg-12">
                    <ul id="progressbar">
                        <li class="active">
                            <a href="/addCompany" class="common-section__title fs-16px title-black">CompanyInfo</a>
                        </li>
                        <li>
                            <a href="/addDirector" class="common-section__title fs-16px title-black">Directors</a>
                        </li>
                        <li>
                            <a href="/addShareHolder" class="common-section__title fs-16px title-black">ShareHolders</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container no-padding">
            <div class="row mr-0px ml-0px">
                <div class="col-md-12 no-padding box-shadow">
                    <div class="row ml-0px mr-0px align-items-center bg-white-smoke common-padding-16px">
                        <div class="col-xl-10">
                            <p class="p-large common-section__title mb-0px">Add company</p>
                        </div>
                        <div class="col-xl-2 text-right no-padding">
                            <a href="/addDirector"
                               class="btn btn-primary custom-btn-portage-green custom-btn-portage-green--outline">
                                Next
                                <span>
                        <i class="fa fa-chevron-right" aria-hidden="true"></i>
                     </span>
                            </a>
                        </div>
                    </div>
                    <div class="row ml-0px mr-0px">
                        <div class="col-md-12 no-padding">
                            <form id="addCompany" method="post" action="{{ url('/addCompanySubmit') }}">
                                <div class="row ml-0px mr-0px common-padding-16px">
                                    <div class="col-md-12 no-padding">
                                        <div class="form-group">
                                            <div class="row ml-0px mr-0px align-items-center">
                                                <div class="col-md-4 text-right">
                                                    <label class="mb-0px common-label">CIN :</label>
                                                </div>
                                                <div class="col-md-5">
                                                    <input type="text" class="form-control common-input__from-control" id="cin_number" name="cin_number" placeholder="Enter CIN" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row ml-0px mr-0px align-items-center">
                                                <div class="col-md-4 text-right">
                                                    <label class="mb-0px common-label">Company Name :</label>
                                                </div>
                                                <div class="col-md-5">
                                                    <input type="text" class="form-control common-input__from-control" id="company_name" name="company_name" placeholder=" Enter Company Name"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row ml-0px mr-0px align-items-center">
                                                <div class="col-md-4 text-right">
                                                    <label class="mb-0px common-label">Company Logo :</label>
                                                </div>
                                                <div class="col-md-5">
                                                    <input type="file" class="form-control common-input__from-control"  id="company_logo" name="company_logo" placeholder=" Enter Company Name"  accept="image/x-png,image/jpeg,image/jpg"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row ml-0px mr-0px align-items-center">
                                                <div class="col-md-4 text-right">
                                                    <label class="mb-0px common-label">Incorporation Date :</label>
                                                </div>
                                                <div class="col-md-5">
                                                    <input type="text" class="form-control common-input__from-control" id="incorporation_date" name="incorporation_date" placeholder=" Enter Incorporation Date"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row ml-0px mr-0px align-items-center">
                                                <div class="col-md-4 text-right">
                                                    <label class="mb-0px common-label">Incorporation City :</label>
                                                </div>
                                                <div class="col-md-5">
                                                    <input type="text" class="form-control common-input__from-control" id="incorporation_city" name="incorporation_city" placeholder="Enter Incorporation City"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row ml-0px mr-0px align-items-center">
                                                <div class="col-md-4 text-right">
                                                    <label class="mb-0px common-label">Website :</label>
                                                </div>
                                                <div class="col-md-5">
                                                    <input type="text" class="form-control common-input__from-control" id="website" name="website" placeholder="Enter Website"/>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                                <div class="row ml-0px mr-0px align-items-center" id="registered__address">
                                    <div class="col-md-12 no-padding">
                                        <div class="row ml-0px mr-0px align-items-center no-padding bg-white-smoke common-padding-16px">
                                            <div class="col-xl-12">
                                                <p class="p-large common-section__title mb-0px">Registered Address</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 no-padding common-padding-16px registered-address_1" >
                                        <div class="form-group">
                                            <div class="row ml-0px mr-0px align-items-center">
                                                <div class="col-md-4 text-right">
                                                    <label class="mb-0px common-label">Address1 :</label>
                                                </div>
                                                <div class="col-md-5">
                                                    <input type="text" class="form-control common-input__from-control address1" id="address1" placeholder="Enter Address1"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row ml-0px mr-0px align-items-center">
                                                <div class="col-md-4 text-right">
                                                    <label class="mb-0px common-label">Address2 :</label>
                                                </div>
                                                <div class="col-md-5">
                                                    <input type="text" class="form-control common-input__from-control address2" id="address2" placeholder="Enter Address2"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row ml-0px mr-0px align-items-center">
                                                <div class="col-md-4 text-right">
                                                    <label class="mb-0px common-label">TelNo's :</label>
                                                </div>
                                                <div class="col-md-5">
                                                    <input type="text" class="form-control common-input__from-control tele_phone" id="tele_phone"  placeholder=" Enter TelNo's "/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row ml-0px mr-0px align-items-center">
                                                <div class="col-md-4 text-right">
                                                    <label class="mb-0px common-label">City :</label>
                                                </div>
                                                <div class="col-md-5">
                                                    <input type="text" class="form-control common-input__from-control city" id="city" placeholder="Enter City"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row ml-0px mr-0px align-items-center">
                                                <div class="col-md-4 text-right">
                                                    <label class="mb-0px common-label">City :</label>
                                                </div>
                                                <div class="col-md-5">
                                                    <input type="text" class="form-control common-input__from-control state" id="state" placeholder="Enter State"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row ml-0px mr-0px align-items-center">
                                                <div class="col-md-4 text-right">
                                                    <label class="mb-0px common-label">City :</label>
                                                </div>
                                                <div class="col-md-5">
                                                    <input type="text" class="form-control common-input__from-control country" id="country" placeholder="Enter country"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row ml-0px mr-0px align-items-center">
                                                <div class="col-md-4 text-right">
                                                    <label class="mb-0px common-label">PINCode :</label>
                                                </div>
                                                <div class="col-md-5">
                                                    <input type="text" class="form-control common-input__from-control pin_code"  id="pin_code" placeholder="PIN Code"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row ml-0px mr-0px align-items-center" id="more-registered__address">

                                </div>
                                <div class="form-group">
                                    <div class="row ml-0px mr-0px align-items-center">
                                        <div class="offset-md-4 col-md-5">
                                            <p class="mb-0px common-section__title cursor-pointer add__more-address"> + Add More address</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row ml-0px mr-0px align-items-center">
                                    <div class="col-md-12 no-padding">
                                        <div class="form-group">
                                            <div class="row ml-0px mr-0px align-items-center">
                                                <div class="col-lg-12">
                                                    <div class="row justify-content-end">
                                                        <div class="col-lg-1">
                                                            <button type="submit"
                                                                    class="btn btn-primary btn-block custom-btn-portage-green custom-btn-portage-green--outline" id="save-company">
                                                                Save
                                                            </button>
                                                        </div>
                                                        <div class="col-lg-1">
                                                            <a href="/companyList"
                                                               class="btn btn-danger btn-block custom-btn-danger custom-btn-danger--outline">
                                                                Cancel</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script src="../../js/company.js"></script>

@endsection