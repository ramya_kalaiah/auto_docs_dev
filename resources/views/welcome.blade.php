<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('headPlugins')
</head>
<body>
<div class="container-fluid no-padding bg-portage-green fixed-top" id="custom-nav">
    @include('Navigation.Navigation')
</div>
<div class="container-fluid no-padding mt-56px">
    <div class="render-page">
        <h1>Dashboard</h1>
    </div>
</div>
<div class="container-fluid no-padding">
    @include('Footer.Footer')
</div>
</body>
</html>
