<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Bootstrap Example</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    
    </head>
    <body>
<!--@if(Session::has('fileName'))
         {{ url("/fileName/".Session::get('fileName')) }}
         
      @endif-->
        <div class="container">
            <div class="header">
            <div class="form-group upload-btn">        
                <h3> Login </h3>
            </div>
                      
            </div>

            @if(Session::has('status'))
                <p class="alert alert-success col-md-5" style="margin:0 auto;">{{ Session::get('status') }}</p>
            @endif
            
            <form class="form-horizontal" action="{{ url('/login') }}" enctype="multipart/form-data" method="post">
                {{ csrf_field() }}

                <div class="form-group">
                    <label class="control-label col-sm-2" for="email">Email Id :</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="email" placeholder="Enter Email" name="emailId" required>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="control-label col-sm-2" for="email">Password :</label>
                    <div class="col-sm-6">
                      <input type="password" name="password" id="password" class="form-control" placeholder="Enter Passowrd" required/>
                    </div>
                </div>
                
                <div class="form-group">        
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-default">Submit</button>
                    </div>
                </div>
                

            </form>
        </div>

</body>
</html>
