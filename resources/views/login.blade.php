<?php ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/_root-fontsize.css"/>
    <link rel="stylesheet" href="/css/_bootstrap-overrides.css"/>
    <link href="/css/Common.css" rel="stylesheet"/>
    <link href="/css/_padding-And-margin.css" rel="stylesheet"/>
    <link href="/css/_typography.css" rel="stylesheet"/>
    <link href="/css/_custom-buttons.css" rel="stylesheet"/>

    <link href="/css/login.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>

<body>
    <div class="container-fluid no-padding min-full-height">
        <div class="row mr-0px ml-0px">
            <div class="col-md-6 no-padding">
                <!-- Slideshow container -->
                <div id="demo" class="carousel slide min-full-height" data-ride="carousel">

                    <!-- Indicators -->
                    <ul class="carousel-indicators">
                        <li data-target="#demo" data-slide-to="0" class="active"></li>
                        <li data-target="#demo" data-slide-to="1"></li>
                        <li data-target="#demo" data-slide-to="2"></li>
                    </ul>

                    <!-- The slideshow -->
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <div class="row ml-0px mr-0px">
                                <div class="col-md-12 no-padding common-flex justify-content-center min-full-height bg-portage-green ">
                                    <h2 class="h2-small common-section__title title-white">Information message 3</h2>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="row ml-0px mr-0px">
                                <div class="col-md-12 no-padding common-flex justify-content-center min-full-height bg-portage-green ">
                                    <h2 class="h2-small common-section__title title-white">Information message 3</h2>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="row ml-0px mr-0px">
                                <div class="col-md-12 no-padding common-flex justify-content-center min-full-height bg-portage-green ">
                                    <h2 class="h2-small common-section__title title-white">Information message 3</h2>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Left and right controls -->
                    <a class="carousel-control-prev" href="#demo" data-slide="prev">
                        <span class="carousel-control-prev-icon"></span>
                    </a>
                    <a class="carousel-control-next" href="#demo" data-slide="next">
                        <span class="carousel-control-next-icon"></span>
                    </a>

                </div>
            </div>
            <div class="col-md-6 no-padding">
                <div class="row ml-0px mr-0px common-flex min-full-height justify-content-center">
                    <div class="col-md-8 no-padding box-shadow pt-32px pb-32px">
                        <div class="row mr-0px ml-0px justify-content-center">
                            <div class="col-md-3 no-padding">
                              <div class="login__logo-container">
                                  <img src="https://gurujana.com/wp-content/themes/guru-jana/images/logo.png" class="login__logo-img"/>
                              </div>
                            </div>
                            
                            @if(Session::has('status'))
                            <div class="col-md-11 no-padding margin-auto">
                                <p class="alert alert-success col-md-6" style="margin:0 auto;">{{ Session::get('status') }}</p>
                            </div>
                            @endif

                            <div class="col-md-11 no-padding margin-auto">
                                <form action="{{ url('/login') }}" enctype="multipart/form-data" method="post">
                                {{ csrf_field() }}    
                                <div class="form-group">
                                        <div class="row ml-0px mr-0px align-items-center">
                                            <div class="col-md-4 text-right">
                                                <label class="mb-0px common-label">Email Id :</label>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control common-input__from-control" id="emailId" name="emailId" placeholder="Enter Email id"/>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row ml-0px mr-0px align-items-center">
                                            <div class="col-md-12 text-right">
                                                <p class="p-small common-section__title title-black mb-0px line-height-0">
                                                    <a href="#">forgot password</a>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row ml-0px mr-0px align-items-center">
                                            <div class="col-md-4 text-right">
                                                <label class="mb-0px common-label">Password :</label>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="password"name="password" id="password" class="form-control common-input__from-control" placeholder="Password"/>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row ml-0px mr-0px align-items-center">
                                            <div class="col-md-12 text-right">
                                                <button type="submit" class="btn btn-primary custom-btn-portage-green custom-btn-portage-green--outline" id="login-sumit">
                                                    submit
</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>