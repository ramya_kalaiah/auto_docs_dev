@extends('Layout.app')

@section('title', 'Template')

@section('body')
    <div class="pt-64px pb-32px">
        <div class="container box-shadow common-padding-32px">
            <div class="row pl-16px pr-16px">
                <div class="col-xl-6">
                    <h5 class="mb-0px common-section__title">Add Template</h5>
                </div>
                <div class="col-xl-6">
                    <form>
                        <div class="form-group">
                            <div class="row ml-0px mr-0px align-items-center">
                                <div class="col-md-6 text-right">
                                    <label class="mb-0px common-label">Agenda:</label>
                                </div>
                                <div class="col-md-6 custom-file">
                                    <input type="file"
                                           class="form-control common-input__from-control custom-file-input "
                                           id="customFile" name="filename">
                                    <label class="custom-file-label" for="customFile">Choose file</label>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="row mt-32px ml-0px mr-0px">
                <div class="col-lg-12 no-padding">
                    <form>
                        <div class="form-group">
                            <div class="row ml-0px mr-0px align-items-center">
                                <div class="col-lg-12">
                                    <h6 class="h6-medium common-section__title mb-16px">Template Name</h6>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row ml-0px mr-0px align-items-center">
                                <div class="col-lg-4">
                                    <label class="common-label">LabelName :</label>
                                    <div class="col-lg-12 no-padding">
                                        <input type="text" class="form-control common-input__from-control"
                                               placeholder="LabelName"/>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <label class="common-label">DropDown :</label>
                                    <div class="col-lg-12 no-padding">
                                        <select class="form-control common-input__from-control selectpicker" data-live-search="true">
                                            <option data-tokens="option1">Option 2</option>
                                            <option data-tokens="option2">Option 3</option>
                                            <option data-tokens="option3">Option 4</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <label class="common-label">MultiSelectDropDown :</label>
                                    <div class="col-lg-12 no-padding">
                                        <select id="SelectMultiId" multiple="multiple" data-live-search="true"
                                                class="form-control common-input__from-control selectpicker">
                                            <option value="option1">Option 2</option>
                                            <option value="option2">Option 3</option>
                                            <option value="option3">Option 4</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group mb-0px">
                            <div class="row ml-0px mr-0px align-items-center">
                                <div class="offset-lg-10 col-lg-2">
                                    <div class="row justify-content-end">
                                        <div class="col-lg-6">
                                            <button type="submit"
                                                    class="btn btn-primary btn-block custom-btn-portage-green custom-btn-portage-green--outline">
                                                Save
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection