@extends('Layout.app')

@section('title', 'Meeting')

@section('body')
    <div class="pt-64px pb-32px">
        <div class="container box-shadow common-padding-32px">
            <div class="row">
                <div class="col-xl-12">
                    <h5 class="mb-0px common-section__title">Add Meeting</h5>
                </div>
            </div>
            <div class="row mt-32px ml-0px mr-0px">
                <div class="col-lg-12 no-padding">
                    <form>
                        <div class="form-group">
                            <div class="row ml-0px mr-0px align-items-center">
                                <div class="col-md-4 text-right">
                                    <label class="mb-0px common-label">User Name :</label>
                                </div>
                                <div class="col-md-5">
                                    <input type="text" class="form-control common-input__from-control"/>
                                </div>

                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row ml-0px mr-0px align-items-center">
                                <div class="col-md-4 text-right">
                                    <label class="mb-0px common-label">EmailId :</label>
                                </div>
                                <div class="col-md-5">
                                    <input type="text" class="form-control common-input__from-control"/>
                                </div>

                            </div>
                        </div>
                        <div class="form-group mb-0px">
                            <div class="row ml-0px mr-0px align-items-center">
                                <div class="offset-lg-4 col-lg-5">
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <button type="submit"
                                                    class="btn btn-primary btn-block custom-btn-portage-green custom-btn-portage-green--outline">
                                                Save
                                            </button>
                                        </div>
                                        <div class="col-lg-3">
                                            <a href="/meetingList"
                                               class="btn btn-danger btn-block custom-btn-danger custom-btn-danger--outline">
                                                Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection