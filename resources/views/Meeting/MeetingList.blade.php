@extends('Layout.app')

@section('title', 'Meeting')

@section('body')
    <div class="pt-64px pb-32px">
        <div class="container box-shadow common-padding-32px">
            <div class="row align-items-center ml-0px mr-0px">
                <div class="col-xl-10 no-padding">
                    <h5 class="mb-0px common-section__title">Meeting List</h5>
                </div>
                <div class="col-xl-2 text-right no-padding">
                    <a href="/addMeeting" class="btn btn-primary custom-btn-portage-green custom-btn-portage-green--outline">
             <span>
                <i class="fa fa-plus" aria-hidden="true"></i>
                </span>
                        Add Meeting
                    </a>
                </div>
            </div>
            <div class="row mt-32px ml-0px mr-0px">
                <div class="col-xl-12 no-padding">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped" id="data-table"  style="width:100%">
                            <thead>
                            <tr class="table-header__container">
                                <th>Meeting</th>
                                <th>EmailId</th>
                                <th>PhoneNumber</th>
                                <th class="width-40px">IsActive</th>
                                <th class="width-80px">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>sai kumar</td>
                                <td>sai@appzoy.com</td>
                                <td>9618010075</td>
                                <td>Yes</td>
                                <td>
                                    <div class="d-flex">
                                        <div><a href="#" class="edit-icon" alt="edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></div>
                                        <div><a href="#" class="trash-icon ml-8px" alt="delete"><i class="fa fa-trash" aria-hidden="true"></i></a></div>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection