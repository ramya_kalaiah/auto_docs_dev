@extends('Layout.app')

@section('title', 'User')

@section('body')
    <div class="pt-64px pb-32px">
        <div class="container box-shadow common-padding-32px">
            <div class="row align-items-center ml-0px mr-0px">
                <div class="col-xl-10 no-padding">
                    <h5 class="mb-0px common-section__title">User List</h5>
                </div>
                <div class="col-xl-2 text-right no-padding">
                    <a href="/addUser" class="btn btn-primary custom-btn-portage-green custom-btn-portage-green--outline">
             <span>
                <i class="fa fa-plus" aria-hidden="true"></i>
                </span>
                        Add User
                    </a>
                </div>
            </div>
            <div class="row mt-32px ml-0px mr-0px"> 
                <div class="col-xl-12 no-padding">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped" id="data-table"  style="width:100%">
                            <thead>
                            <tr class="table-header__container">
                                <th>User Name</th>
                                <th>Email Id</th>
                                <th>Phone Number</th>
                                <th class="width-40px">Status</th>
                                <th class="width-80px">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                            <tr>
                                <td>{{$user->user_name}}</td>
                                <td>{{$user->email_id}}</td>
                                <td>{{$user->phone_number}}</td>
                                <td>{{$user->status}}</td>
                                <td>
                                    <div class="d-flex">
                                        <div><a href="/editUser/{{$user->id}}" class="edit-icon" alt="edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></div>
                                        <div><a href="/deleteUser/{{$user->id}}" class="trash-icon ml-8px" alt="delete"><i class="fa fa-trash" aria-hidden="true"></i></a></div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection