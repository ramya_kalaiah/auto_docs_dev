@extends('Layout.app')

@section('title', 'User')

@section('body')
    <div class="pt-64px pb-32px">
        <div class="container box-shadow common-padding-32px">
            <div class="row">
                <div class="col-xl-12">
                    <h5 class="mb-0px common-section__title">Add User</h5>
                </div>
            </div>
            @if(Session::has('status'))
                <p class="alert alert-success col-md-5" style="margin:0 auto;">{{ Session::get('status') }}</p>
            @endif
            <div class="row mt-32px ml-0px mr-0px">
                <div class="col-lg-12 no-padding">
                    <form id="addUser" method="post" action="{{ url('/addUserSubmit') }}" >
                    {{ csrf_field() }}
                        <div class="form-group">
                            <div class="row ml-0px mr-0px align-items-center">
                                <div class="col-md-4 text-right">
                                    <label class="mb-0px common-label">User Name<span>*</span> :</label>
                                </div>
                                <div class="col-md-5">
                                    <input type="text" name="userName" id="userName" class="form-control common-input__from-control" placeholder="User Name" required/>
                                </div>

                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="row ml-0px mr-0px align-items-center">
                                <div class="col-md-4 text-right">
                                    <label class="mb-0px common-label">Select Role<span>*</span> :</label>
                                </div>
                                <div class="col-md-5">
                                    <select class="form-control common-input__from-control selectpicker" id="selectRole" name="selectRole" data-live-search="true">
                                        <option data-tokens="option0" selected disabled>Select Role</option>
                                        @foreach($roles as $role)
                                        <option data-tokens="option1" value='{{$role->id}}'>{{$role->roles}}</option>
                                        @endforeach
                                    </select>     
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row ml-0px mr-0px align-items-center">
                                <div class="col-md-4 text-right">
                                    <label class="mb-0px common-label">Set Password<span>*</span> :</label>
                                </div>
                                <div class="col-md-5">
                                    <input type="password" name="password" id="password" class="form-control common-input__from-control" placeholder="Enter Passowrd" required/>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row ml-0px mr-0px align-items-center">
                                <div class="col-md-4 text-right">
                                    <label class="mb-0px common-label">Email Id<span>*</span> :</label>
                                </div>
                                <div class="col-md-5">
                                    <input type="email" name="email" id="email" class="form-control common-input__from-control" placeholder="email Id" required/>
                                </div>

                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row ml-0px mr-0px align-items-center">
                                <div class="col-md-4 text-right">
                                    <label class="mb-0px common-label">Phone Number<span>*</span> :</label>
                                </div>
                                <div class="col-md-5">
                                    <input type="number" id="phoneNumber" name="phoneNumber" class="form-control common-input__from-control" placeholder="Phone Number" required/>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row ml-0px mr-0px align-items-center">
                                <div class="col-md-4 text-right">
                                    <label class="mb-0px pt-7px common-label">Is Active<span>*</span> :</label>
                                </div>
                                <div class="col-md-5 d-inline-flex">
                                    <label class="radio__container">Yes
                                        <input type="radio" checked="checked" id="active" name="radio" value="active">
                                        <span class="radio-checkmark"></span>
                                    </label>
                                    <label class="radio__container">No
                                        <input type="radio" name="radio" id="inActive" value="inActive">
                                        <span class="radio-checkmark"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group mb-0px">
                            <div class="row ml-0px mr-0px align-items-center">
                                <div class="offset-lg-4 col-lg-5">
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <button type="submit" 
                                                    class="btn btn-primary btn-block custom-btn-portage-green custom-btn-portage-green--outline" id="save-user">
                                                Save
                                            </button>
                                        </div>
                                        <div class="col-lg-3">
                                            <a href="/userList"
                                               class="btn btn-danger btn-block custom-btn-danger custom-btn-danger--outline" id="cancel-user">
                                                Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script src="../../js/user.js"></script>
@endsection