@extends('Layout.app')

@section('title', 'UiComponent')

@section('body')
    <div class="pt-32px pb-32px">
        <div class="container box-shadow common-padding-32px">
            <div class="row ml-0px mr-0px align-items-center">
                <div class="col-xl-12">
                    <h5 class="mb-0px common-section__title">Ui Component</h5>
                </div>
            </div>
            <div class="row mt-32px ml-0px mr-0px">
                <div class="col-lg-12 no-padding">
                    <form method="POST" action="#" enctype="multipart/form-data">
                        <div class="form-group">
                            <div class="row ml-0px mr-0px align-items-center"> 
                                <div class="col-lg-4">
                                    <label class="common-label">LabelName :</label>
                                    <div class="col-lg-12 no-padding">
                                        <input type="text" class="form-control common-input__from-control"
                                               placeholder="LabelName"/>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <label class="common-label">DropDown :</label>
                                    <div class="col-lg-12 no-padding">
                                        <select class="form-control common-input__from-control selectpicker" data-live-search="true">
                                            <option data-tokens="option1">Option 2</option>
                                            <option data-tokens="option2">Option 3</option>
                                            <option data-tokens="option3">Option 4</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <label class="common-label">MultiSelectDropDown :</label>
                                    <div class="col-lg-12 no-padding">
                                        <select id="SelectMultiId" multiple="multiple" data-live-search="true"
                                                class="form-control common-input__from-control selectpicker">
                                            <option value="option1">Option 2</option>
                                            <option value="option2">Option 3</option>
                                            <option value="option3">Option 4</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row ml-0px mr-0px align-items-center">
                                <div class="col-lg-4">
                                    <label class="common-label">textArea :</label>
                                    <div class="col-lg-12 no-padding">
                                        <textarea type="text" class="form-control common-input__from-control"
                                                  placeholder="textArea"></textarea>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <label class="common-label">DatePicker :</label>
                                    <div class="col-lg-12 no-padding input-group date" id="datepicker" data-date-format="mm-dd-yyyy">
                                        <input type="text" class="form-control common-input__from-control"
                                               placeholder="DatePicker"/>
                                        <span class="input-group-addon common-input-group__btn"><i class="glyphicon glyphicon-calendar"></i></span>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <label class="common-label">timePicker :</label>
                                        <div class="col-lg-12 no-padding input-group bootstrap-timepicker timepicker">
                                            <input id="timepicker1" type="text" class="form-control input-small common-input__from-control">
                                            <span class="input-group-addon common-input-group__btn"><i class="glyphicon glyphicon-time"></i></span>
                                        </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row ml-0px mr-0px align-items-center">
                                <div class="col-lg-4">
                                    <label class="common-label">File</label>
                                    <div class="col-lg-12 custom-file">
                                        <input type="file" class="form-control common-input__from-control custom-file-input " id="customFile" name="filename">
                                        <label class="custom-file-label" for="customFile">Choose file</label>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <label class="common-label">Radio type :</label>
                                    <div class="col-lg-12 no-padding">
                                        <div class="col-lg-12 no-padding d-flex">
                                            <label class="radio__container">Yes
                                                <input type="radio" checked="checked" name="radio">
                                                <span class="radio-checkmark"></span>
                                            </label>
                                            <label class="radio__container">No
                                                <input type="radio" name="radio">
                                                <span class="radio-checkmark"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.selectpicker').selectpicker();
        /*    $('#SelectMultiId').multiselect({
                includeSelectAllOption: true,
                enableFiltering: true

            });*/
            $('#datepicker').datepicker({
                format: 'mm/dd/yyyy',
                //startDate: '-3d',
                autoclose: true,
            });
            $('#timepicker1').timepicker();

        });
    </script>

@endsection