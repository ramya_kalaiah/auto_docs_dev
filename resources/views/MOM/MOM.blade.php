@extends('Layout.app')

@section('title', 'document')

@section('body')
    <div class="pt-32px pb-32px">
        <div class="container common-padding-32px">
            <div class="row">
                <div class="col-lg-12">
                    <ul id="progressbar" class="progressbar--width">
                        <li class="active">
                            <a href="/document" class="common-section__title fs-16px title-black">Setting</a>
                        </li>
                        <li class="active">
                            <a href="/attendanceSheet" class="common-section__title fs-16px title-black">Attendance
                                Sheet</a>
                        </li>
                        <li class="active">
                            <a href="/MOM" class="common-section__title fs-16px title-black">MOM</a>
                        </li>
                        <li>
                            <a href="#" class="common-section__title fs-16px title-black">Notice &
                                Notices</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container box-shadow common-padding-32px">
            <div class="row ml-0px mr-0px align-items-center">
                <div class="col-xl-10">
                    <h5 class="mb-0px common-section__title">MOM</h5>
                </div>
                <div class="col-xl-2 text-right no-padding">
                    <a href="#"
                       class="btn btn-primary custom-btn-portage-green custom-btn-portage-green--outline">
                        Next
                        <span>
                        <i class="fa fa-chevron-right" aria-hidden="true"></i>
                     </span>
                    </a>
                </div>
            </div>
            <div class="row mt-32px ml-0px mr-0px">
                <div class="col-lg-12 no-padding">
                    <form>
                        <div class="form-group">
                            <div class="row ml-0px mr-0px align-items-center">
                                <div class="col-lg-4">
                                    <label class="common-label">Financial Year :</label>
                                    <div class="col-lg-12 no-padding">
                                        <input type="text" class="form-control common-input__from-control"
                                               placeholder="Financial year"/>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <label class="common-label">MeetingID :</label>
                                    <div class="col-lg-12 no-padding">
                                        <input type="text" class="form-control common-input__from-control"
                                               placeholder="MeetingID"/>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <label class="common-label">Sections :</label>
                                    <div class="col-lg-12 no-padding">
                                        <select id="SelectMultiId" multiple="multiple" data-live-search="true"
                                                class="form-control common-input__from-control selectpicker">
                                            <option value="option1">Option 2</option>
                                            <option value="option2">Option 3</option>
                                            <option value="option3">Option 4</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group mb-0px">
                            <div class="row ml-0px mr-0px align-items-center">
                                <div class="col-lg-12">
                                    <div class="row justify-content-end">
                                        <div class="col-lg-1">
                                            <button type="submit"
                                                    class="btn btn-primary btn-block custom-btn-portage-green custom-btn-portage-green--outline">
                                                Save
                                            </button>
                                        </div>
                                        <div class="col-lg-1">
                                            <a href="/companyList"
                                               class="btn btn-danger btn-block custom-btn-danger custom-btn-danger--outline">
                                                Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.selectpicker').selectpicker();

        });
    </script>

@endsection