<div class="container no-padding">
    <nav class="navbar navbar-expand-lg no-padding">
        <a class="navbar-brand" href="#">
            <div class="d-flex">
                <div class="width-64px">
                    <img src="https://gurujana.com/wp-content/themes/guru-jana/images/logo.png" class="log-img__container"/>
                </div>
                <div class="width-64px">
                    <img src="https://gurujana.com/wp-content/themes/guru-jana/images/Logo-1.png" class="log-img__container"/>
                </div>
            </div>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
            
            <?php if(Session::get('roleId') == '1'){  ?>
                <li class="nav-item">
                    <a class="nav-link  nav-link__item" href="/userList">User</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link  nav-link__item" href="/companyList">Company Info</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link  nav-link__item" href="/document">Document</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link  nav-link__item" href="/meetingList">Meeting</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link  nav-link__item" href="/template">Template</a>
                </li> 
            <?php } else if(Session::get('roleId') == '2') { ?>
                <li class="nav-item">
                    <a class="nav-link  nav-link__item" href="/companyList">Company Info</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link  nav-link__item" href="/document">Document</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link  nav-link__item" href="/meetingList">Meeting</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link  nav-link__item" href="/template">Template</a>
                </li> 
                
                <?php 

            } else if(Session::get('roleId') == '3'){ ?>
                <li class="nav-item">
                    <a class="nav-link  nav-link__item" href="/document">Document</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link  nav-link__item" href="/template">Template</a>
                </li> 
                <?php } ?>
            </ul>
            <div class="dropdown">
                <button type="button"
                        class="btn btn-primary d-flex align-items-center dropdown-toggle profile__dropdown-btn no-padding"
                        data-toggle="dropdown">
                    <div class="common-flex">
                        <div>
                            <p class="p-medium mb-0px pr-8px text-capitalize">{{Session::get('userName')}}</p>
                        </div>
                        <div class="profile-img__container">
                            <img src="/img/profileimg.jpg" class="rounded-circle profile-img"/>
                            <!-- <i class="fa fa-user-circle fs-32px"></i> -->
                        </div>
                    </div>
                </button>
                <div class="dropdown-menu dropdown-menu-right pl-8px pr-8px mt-8px">
                    <div>
                        <div class="profile-img__container m-auto">
                            <img src="/img/profileimg.jpg" class="rounded-circle profile-img"/>
                        </div>
                        <div class="pt-8px pb-8px">
                            <p class="p-small mb-0px text-capitalize text-center">{{Session::get('userName')}}</p>
                            <p class="p-small mb-0px text-center">{{Session::get('emailId')}}</p>
                        </div>
                    </div>
                    <div class="dropdown-divider"></div>
                    <div>
                        <p class="p-medium mb-0px text-center">SignOut</p>
                    </div>
                </div>
            </div>
        </div>
    </nav>
</div>