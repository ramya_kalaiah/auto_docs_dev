<div class="container no-padding">
    <nav class="navbar navbar-expand-lg no-padding">
        <a class="navbar-brand" href="#">
            <div class="d-flex">
                <div class="width-64px">
                    <img src="https://gurujana.com/wp-content/themes/guru-jana/images/logo.png" class="log-img__container"/>
                </div>
                <div class="width-64px">
                    <img src="https://gurujana.com/wp-content/themes/guru-jana/images/Logo-1.png" class="log-img__container"/>
                </div>
            </div>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse pt-48px" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link  nav-link__item" href="/userList">User</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link  nav-link__item" href="/companyList">Company Info</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link  nav-link__item" href="/document">Document</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link  nav-link__item" href="/meetingList">Meeting</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link  nav-link__item" href="/template">Template</a>
                </li>
            </ul>
            <ul class="navbar-nav align-items-center">
                <li class="nav-item">
                    <a class="nav-link  nav-link__item">SuperAdmin@gmail.com</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link  nav-link__item" href="#">Logout</a>
                </li>
            </ul>
        </div>
    </nav>
</div>