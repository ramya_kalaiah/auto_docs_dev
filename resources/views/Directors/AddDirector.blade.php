@extends('Layout.app')

@section('title', 'Director')

@section('body')
    <div class="pt-32px pb-32px">
        <div class="container common-padding-32px">
            <div class="row">
                <div class="col-lg-12">
                    <ul id="progressbar">
                        <li class="active">
                            <a href="/addCompany" class="common-section__title fs-16px title-black">CompanyInfo</a>
                        </li>
                        <li class="active">
                            <a href="/addDirector" class="common-section__title fs-16px title-black">Directors</a>
                        </li>
                        <li>
                            <a href="/addShareHolder" class="common-section__title fs-16px title-black">ShareHolders</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container box-shadow common-padding-32px">
            <div class="row ml-0px mr-0px align-items-center">
                <div class="col-xl-10">
                    <h5 class="mb-0px common-section__title">Add Director</h5>
                </div>
                <div class="col-xl-1 text-right no-padding">
                    <a href="/addCompany" class="btn btn-primary custom-btn-portage-green custom-btn-portage-green--outline">
                      <span>
                        <i class="fa fa-chevron-left" aria-hidden="true"></i>
                     </span>
                        Prev
                    </a>
                </div>
                <div class="col-xl-1 text-right no-padding">
                    <a href="/addShareHolder" class="btn btn-primary custom-btn-portage-green custom-btn-portage-green--outline">
                        Next
                        <span>
                        <i class="fa fa-chevron-right" aria-hidden="true"></i>
                     </span>
                    </a>
                </div>
            </div>
            <div class="row mt-32px ml-0px mr-0px">
                <div class="col-lg-12 no-padding">
                    <form>
                        <div class="form-group">
                            <div class="row ml-0px mr-0px align-items-center">
                                <div class="col-lg-4">
                                    <label class="common-label">DirectorName :</label>
                                    <div class="col-lg-12 no-padding">
                                        <input type="text" class="form-control common-input__from-control" placeholder="DirectorName"/>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <label class="common-label">DateOfBrith :</label>
                                    <div class="col-lg-12 no-padding">
                                        <input type="text" class="form-control common-input__from-control" placeholder="DateOfBrith"/>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <label class="common-label">Gender :</label>
                                    <div class="col-lg-12 no-padding">
                                        <select class="form-control common-input__from-control">
                                            <option>--select--</option>
                                            <option>Male</option>
                                            <option>Female</option>
                                            <option>other</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row ml-0px mr-0px align-items-center">
                                <div class="col-lg-4">
                                    <label class="common-label">Upload Image</label>
                                    <div class="col-lg-12 custom-file">
                                        <input type="file" class="form-control common-input__from-control custom-file-input " id="customFile" name="filename">
                                        <label class="custom-file-label" for="customFile">Choose file</label>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <label class="common-label">DIN :</label>
                                    <div class="col-lg-12 no-padding">
                                        <input type="text" class="form-control common-input__from-control" placeholder="DIN"/>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <label class="common-label">Designation :</label>
                                    <div class="col-lg-12 no-padding">
                                        <input type="text" class="form-control common-input__from-control" placeholder="Designation"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row ml-0px mr-0px align-items-center">
                                <div class="col-lg-4">
                                    <label class="common-label">Address1 :</label>
                                    <div class="col-lg-12 no-padding">
                                        <input type="text" class="form-control common-input__from-control" placeholder="Address1"/>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <label class="common-label">Address2 :</label>
                                    <div class="col-lg-12 no-padding">
                                        <input type="text" class="form-control common-input__from-control" placeholder="Address2"/>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <label class="common-label">City :</label>
                                    <div class="col-lg-12 no-padding">
                                        <input type="text" class="form-control common-input__from-control" placeholder="City"/>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row ml-0px mr-0px align-items-center">
                                <div class="col-lg-4">
                                    <label class="common-label">PINCode :</label>
                                    <div class="col-lg-12 no-padding">
                                        <input type="text" class="form-control common-input__from-control" placeholder="PINCode"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group mb-0px">
                            <div class="row ml-0px mr-0px align-items-center">
                                <div class="col-lg-12">
                                    <div class="row justify-content-end">
                                        <div class="col-lg-1">
                                            <button type="submit"
                                                    class="btn btn-primary btn-block custom-btn-portage-green custom-btn-portage-green--outline">
                                                Save
                                            </button>
                                        </div>
                                        <div class="col-lg-1">
                                            <a href="/companyList"
                                               class="btn btn-danger btn-block custom-btn-danger custom-btn-danger--outline">
                                                Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection